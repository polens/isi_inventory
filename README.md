# ISI Equipment Inventory

From ISI Hackathon dated Sep 3, 2016:

> Inventory System Project
>
> Currently, records on equipments purchased and assigned to employees are found in a spreadsheet. Each tab or worksheet has data for each employee on what equipments are assigned to him or her. Assigning or reassigning an equipment to an employee means manually encoding or copy-pasting data into a worksheet. This process is time-consuming and can be prone to data entry errors.
>
> Thus, we propose a system that is more convenient to all stakeholders that is less prone to errors. The system or application should enable to do the following.
>
> * Add new equipments in the inventory list
> * Assign and re-assign equipments to employees or storage spaces
> * Able to specify details like brand or model for a particular type of equipment
> * Update state on equipments, such as if it is defective or no longer working
> * Enable to view history of an equipment (date and details on when it was added, assigned, re-assigned, found defective, etc.)
> * Enable to add photos

## Project Structure

Apps:

* `employees` - holds all the modules for Employee related code i.e. (models, views, serializers, etc.)
* `equipments` - holds all the modules for Equipment related code i.e. (models, views, serializers, etc.)
* `export` - holds all the modules for export related tasks (exporting to local file, Google Spreadsheets, etc.)
* `import` - holds all the modules for import related tasks (importing from local file, Google Spreadsheets, etc.)
* `suppliers` - holds all the modules for Supplier related code i.e. (models, views, serializers, etc.)

Folders:

* `data/raw` - holds all the client provided data files e.g. ISI Fixed Assets.xlsx
* `data/sanitized` - holds all the correctly formatted data files to be used for importing
* `tests` - holds all the tests to be run by the Django test suite
