from rest_framework import serializers

from .models import Employee


class EmployeeSerializer(serializers.ModelSerializer):
    equipments = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='api:equipment-detail')

    class Meta:
        model = Employee
        fields = ('id_number', 'name', 'email', 'equipments')