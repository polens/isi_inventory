from __future__ import unicode_literals

from django.db import models


class Employee(models.Model):
    id_number = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=128, default='', blank=False, null=False)
    email = models.EmailField(default='', null=True, blank=True)

    def __unicode__(self):
        return '{} ({})'.format(self.name, self.id_number)
