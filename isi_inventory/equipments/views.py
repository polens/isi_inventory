from rest_framework import permissions, viewsets

from .models import Equipment, EquipmentBrand
from .serializers import EquipmentSerializer, EquipmentBrandSerializer


class EquipmentViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Equipment.objects.all()
    serializer_class = EquipmentSerializer


class EquipmentBrandViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = EquipmentBrand.objects.all()
    serializer_class = EquipmentBrandSerializer
