from __future__ import unicode_literals

from django.conf import settings
from django.db import models
from django.utils.timezone import now


class EquipmentBrand(models.Model):
    name = models.CharField(max_length=45, default='', blank=False, null=False)

    def __unicode__(self):
        return self.name


class EquipmentImage(models.Model):
    image = models.ImageField()
    caption = models.TextField()


class Equipment(models.Model):
    name = models.CharField(max_length=255, default='', blank=False, null=False)
    property_number = models.CharField(max_length=45, default='', blank=True, null=True)
    serial_number = models.CharField(max_length=45, default='', blank=True, null=True)
    model = models.CharField(max_length=255, default='', blank=True, null=True)
    location = models.CharField(max_length=255, default='')
    images = models.ManyToManyField(EquipmentImage, blank=True)
    cost = models.DecimalField(max_digits=12, decimal_places=2, default=0.00, blank=False, null=False)
    purchase_date = models.DateField(default=now, blank=True, null=True)
    estimated_useful_life = models.IntegerField(default=12, blank=False, null=False)
    brand = models.ForeignKey(EquipmentBrand, related_name='equipments')
    assignee = models.ForeignKey('employees.Employee', blank=True, null=True, related_name='equipments')
    supplier = models.ForeignKey('suppliers.Supplier', related_name='equipments')


    def __unicode__(self):
        return self.name


class EquipmentHistory(models.Model):
    event = models.CharField(max_length=50, choices=settings.EQUIPMENT_HISTORY_EVENTS, blank=False, null=False)
    event_date = models.DateField(default=now, blank=True, null=True)
    notes = models.TextField()
    equipment = models.ForeignKey(Equipment, related_name='history')

    def __unicode__(self):
        return '{} {} {}'.format(self.equipment, self.event, self.event_date)
