from django import forms
from django.conf import settings
from django.contrib import admin
from django.utils.timezone import now

from .models import EquipmentBrand, Equipment, EquipmentHistory


class EquipmentHistoryForm(forms.ModelForm):
    event = forms.ChoiceField(choices=settings.EQUIPMENT_HISTORY_EVENTS)
    event_date = forms.DateField(required=False, initial=now)
    notes = forms.CharField(required=False)

    class Meta:
        model = Equipment
        fields = '__all__'

    def save(self, commit=True):
        obj = super(EquipmentHistoryForm, self).save(commit)
        obj.save()

        EquipmentHistory.objects.create(event=self.cleaned_data['event'],
                                        notes=self.cleaned_data['notes'],
                                        event_date=self.cleaned_data['event_date'],
                                        equipment=obj)

        return obj


class EquipmentHistoryInline(admin.TabularInline):
    model = EquipmentHistory
    readonly_fields = ('event_date', 'event', 'notes')
    can_delete = False
    extra = 0
    max_num = 0
    ordering = ('-id',)


class EquipmentAdmin(admin.ModelAdmin):
    list_filter = ('assignee',)
    list_display = ('name', 'property_number', 'serial_number', 'model', 'location', 'brand', 'assignee')
    search_fields = ('name', 'property_number', 'serial_number', 'model', 'location', 'brand__name', 'assignee__name',
                     'assignee__id_number')
    inlines = [EquipmentHistoryInline]
    form = EquipmentHistoryForm


admin.site.register(Equipment, EquipmentAdmin)
admin.site.register(EquipmentBrand)
