from rest_framework import serializers

from .models import Equipment, EquipmentBrand


class EquipmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Equipment


class EquipmentBrandSerializer(serializers.ModelSerializer):
    equipments = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='api:equipment-detail')

    class Meta:

        model = EquipmentBrand
        fields = ('id', 'name', 'equipments')
