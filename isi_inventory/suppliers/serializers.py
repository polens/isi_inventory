from rest_framework import serializers

from .models import Supplier


class SupplierSerializer(serializers.ModelSerializer):
    equipments = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='api:equipment-detail')

    class Meta:
        model = Supplier
