from __future__ import unicode_literals

from django.db import models


class Supplier(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    address = models.TextField(default='', blank=False, null=False)
    phone_number = models.CharField(max_length=25, default='', blank=False, null=False)
    blacklisted = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name
